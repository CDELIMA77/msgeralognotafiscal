package br.com.mastertech.GeraLogNotaFiscal;

import br.com.mastertech.SolicitacaoNotaFiscal.models.Log;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;

@Component
public class WriteToFile {

    public static void gravaLog(Log log) {
        try {
            FileWriter myWriter = new FileWriter("LogNfe.txt", true);
            myWriter.append(log.getLog());
            myWriter.append(System.getProperty("line.separator"));
            myWriter.flush();
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}



