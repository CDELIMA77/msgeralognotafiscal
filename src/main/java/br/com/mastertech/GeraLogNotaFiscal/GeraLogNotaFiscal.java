package br.com.mastertech.GeraLogNotaFiscal;

import br.com.mastertech.SolicitacaoNotaFiscal.models.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class GeraLogNotaFiscal {
    @Autowired
    WriteToFile writeToFile;

    @KafkaListener(topics = "spec2-cynthia-carvalho-2", groupId = "grupo-2")
    public void receber(@Payload Log log) {
        writeToFile.gravaLog(log);
    }
}
